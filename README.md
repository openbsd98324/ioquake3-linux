# ioquake3-linux

Source and compiled binary for Quake3, i.e. ioquake3, running on Linux 


## Installation 

1.) ROOTFS / BASE 

Available rootfs with linux devuan (ascii) base, with firmware: 

https://gitlab.com/openbsd98324/devuan-rescue/-/raw/main/ascii/DEVUAN-RESCUE.tar.gz

https://gitlab.com/openbsd98324/devuan-rescue/-/raw/main/ascii/DEVUAN-RESCUE.tar.gz.md5

2.) Install the SDL2 library, and prefered env. 

     See for instance FLTK FLEVILWM and FLDE desktop

     See also Blackbox... 


3.) Copy the necessary ioquake3 content 

https://gitlab.com/openbsd98324/ioquake3-linux/-/raw/main/binary/linux/devuan/ascii/amd64/ioquake3-debian-master-compiled-devuan-ascii-v1.tar.gz



## Q3A Directory 

```
echo PAK FILES
echo ---------
echo "Copy the necessary files into ~/.q3a" 
```

The Demo Pack files:
http://files.retropie.org.uk/archives/Q3DemoPaks.zip


Mirror: 

https://gitlab.com/openbsd98324/ioquake3-baseq3-demo





```
home/pi/.q3a/baseq3/
home/pi/.q3a/baseq3/pak4.pk3
home/pi/.q3a/baseq3/pak2.pk3
home/pi/.q3a/baseq3/pak1.pk3
home/pi/.q3a/baseq3/pak6.pk3
home/pi/.q3a/baseq3/pak0.pk3
home/pi/.q3a/baseq3/pak8.pk3
home/pi/.q3a/baseq3/pak7.pk3
home/pi/.q3a/baseq3/pak5.pk3
home/pi/.q3a/baseq3/pak3.pk3
```



## Run and Play 


On AMD64, just run the command line: 

ioquake3-debian/ioq3/build/release-linux-x86_64/ioquake3.x86_64

or 

/usr/local/bin/ioquake3.x86_64


```
echo ARM like Raspberry PI 
echo ---------------------
echo "ioquake3-debian/ioq3/build/release-linux-arm/ioquake3.arm"



echo AMD64 
echo -----
echo "ioquake3-debian/ioq3/build/release-linux-x86_64/ioquake3.x86_64" 
```

# Screenshots

![](media/Quake_III_Arena_q3dm0.png)


![](media/quake-iii-running-on-pi.png)











